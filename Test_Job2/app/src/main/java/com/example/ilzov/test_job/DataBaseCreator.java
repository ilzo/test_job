package com.example.ilzov.test_job;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by ilzov on 27.10.15.
 */
public class DataBaseCreator extends SQLiteOpenHelper {
    public static final String DB_NAME = "db_note";
    public static final int DB_VERSION = 1;

    public static class Notes implements BaseColumns {
        public static final String TABLE_NAME = "t_notes";
        public static final String MAIN_TITLE = "title";
        public static final String MAIN_NOTE = "note";
        public static final String MAIN_SHORTDES = "icon";

    }
// Ctreate notes table script
    static String SCRIPT_CREATE_TBL_MAIN = "CREATE TABLE " + Notes.TABLE_NAME + " (" + Notes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Notes.MAIN_TITLE + " TEXT UNIQUE, " +Notes.MAIN_NOTE+ " TEXT, "+Notes.MAIN_SHORTDES+" TEXT );";

    public DataBaseCreator(Context context) { super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCRIPT_CREATE_TBL_MAIN);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

