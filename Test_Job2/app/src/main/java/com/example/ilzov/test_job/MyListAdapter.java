package com.example.ilzov.test_job;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ilzov on 26.10.15.
 */
public class MyListAdapter extends BaseAdapter {
    private ArrayList<MyListItem> items=new ArrayList<MyListItem>();

    public MyListAdapter(ArrayList<MyListItem> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public MyListItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.list_item_layout, parent, false);
        }
        TextView title= (TextView) convertView.findViewById(R.id.title_textView);
        title.setText(items.get(position).getTitle().toString());
        TextView description= (TextView) convertView.findViewById(R.id.description_textView);
        description.setText(items.get(position).getDescription().toString());
        ImageView image= (ImageView) convertView.findViewById(R.id.image_imageView);
        image.setImageResource(R.drawable.bitmap);


        return convertView;
    }
}
