package com.example.ilzov.test_job;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button addNoteButton;
    private ListView notesListView;
    private MyListAdapter adapter;
    private ArrayList<MyListItem> items=new ArrayList<>();
    //private SQLiteDataBaseManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//Get data from DB
        items=SQLiteDataBaseManager.getInstance(MainActivity.this).getUsers();
//Initilize views
        addNoteButton= (Button) findViewById(R.id.add_button);
        notesListView= (ListView) findViewById(R.id.listView);

        if (items.size()!=0) {
            adapter = new MyListAdapter(items);
            notesListView.setAdapter(adapter);
        }
//Set listeners
        addNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewNoteActivity.class));
            }
        });
        notesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, NoteViewActivity.class);
                intent.putExtra("note", adapter.getItem(position).getNote());
                intent.putExtra("title", adapter.getItem(position).getTitle());
                intent.putExtra("descript", adapter.getItem(position).getDescription());
                startActivity(intent);
            }
        });
        notesListView.setLongClickable(true);
        notesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           final int pos, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Allart");
                builder.setMessage("Do you sure want delete this note ?");
                builder.setCancelable(true);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //dialog.dismiss();
                        SQLiteDataBaseManager.getInstance(MainActivity.this)
                                .removeNote(adapter.getItem(pos).getTitle());
                        recreate();
                    }
                });
                AlertDialog alert=builder.create();
                alert.show();
                return true;
            }
        });

    }
    private void reloadList(){
        items=SQLiteDataBaseManager.getInstance(MainActivity.this).getUsers();
        if (items.size()!=0) {
            adapter.notifyDataSetChanged();
            //notesListView.setAdapter(adapter);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        reloadList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
