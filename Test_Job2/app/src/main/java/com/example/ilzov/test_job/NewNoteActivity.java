package com.example.ilzov.test_job;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewNoteActivity extends AppCompatActivity {
    private Button addButton;
    private EditText newTitle, newNote, newShortDescript;

    private String title="";
    private String note="";
    private String shortDescript="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//Initilize views
        addButton= (Button) findViewById(R.id.add_note_button);
        newTitle= (EditText) findViewById(R.id.new_title_editText);
        newNote= (EditText) findViewById(R.id.new_note_editText);
        newShortDescript= (EditText) findViewById(R.id.sub_description_editText);
// Set listeners
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title=newTitle.getText().toString();
                note=newNote.getText().toString();
                shortDescript=newShortDescript.getText().toString();
                if (!title.equals("")){
                    if(SQLiteDataBaseManager.getInstance(NewNoteActivity.this).insertNote(title,
                            note,shortDescript)>0){
                        Intent intent=new Intent(NewNoteActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Toast.makeText(NewNoteActivity.this,"Note with same title already exist!",Toast.LENGTH_LONG).show();
                        newTitle.setText("");
                    }
                }
            }
        });

    }

}
