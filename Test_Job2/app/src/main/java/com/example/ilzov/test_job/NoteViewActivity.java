package com.example.ilzov.test_job;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class NoteViewActivity extends AppCompatActivity {
    private TextView noteText,descriptText;
    private ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//Initilize views
        setTitle(getIntent().getStringExtra("title"));
        descriptText= (TextView) findViewById(R.id.descript_textView);
        descriptText.setText(getIntent().getStringExtra("descript"));
        noteText= (TextView) findViewById(R.id.note_view_textView);
        noteText.setText(getIntent().getStringExtra("note"));
        icon= (ImageView) findViewById(R.id.icon_imageView);
        icon.setImageResource(R.drawable.bitmap);

    }

}
