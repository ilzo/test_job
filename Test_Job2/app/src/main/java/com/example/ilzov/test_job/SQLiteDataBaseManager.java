package com.example.ilzov.test_job;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by ilzov on 27.10.15.
 */
public class SQLiteDataBaseManager {
    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;
    private static SQLiteDataBaseManager instance;

    private SQLiteDataBaseManager(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database==null || !database.isOpen()) {
        database = dbCreator.getWritableDatabase();
        }
        Log.d("<< DB state >>","  dtabase "+database.isOpen());
    }
    public static SQLiteDataBaseManager getInstance(Context context) {
        if (instance==null) {
        instance = new SQLiteDataBaseManager(context);
        }
        return instance;
    }
//Add notes in DB
    public long insertNote(String title, String note, String descript) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseCreator.Notes.MAIN_TITLE, title);
        cv.put(DataBaseCreator.Notes.MAIN_NOTE, note);
        cv.put(DataBaseCreator.Notes.MAIN_SHORTDES,descript);
        return database.insert(DataBaseCreator.Notes.TABLE_NAME, null, cv);
    }
//Get all notes from DB
    public ArrayList<MyListItem> getUsers() {
        Cursor cursor = database.query(DataBaseCreator.Notes.TABLE_NAME, null, null, null, null, null, null);
        ArrayList<MyListItem> list = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(new MyListItem(cursor.getString(1),cursor.getString(3),cursor.getString(2)));
            Log.d("<< Get data from DB >>", " " + cursor.getString(1)+cursor.getString(2)+cursor.getString(3));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
//Remove note from DB
    public void removeNote(String title){

        int result=database.delete(DataBaseCreator.Notes.TABLE_NAME,
                DataBaseCreator.Notes.MAIN_TITLE+ " = ?",new String[]{title});
        Log.d("<< delete from DB >>"," "+result);
    }
}
