package com.example.ilzov.test_job;

import java.io.Serializable;

/**
 * Created by ilzov on 26.10.15.
 */
public class MyListItem implements Serializable {
    private String title;
    private String description;
    private String note;

    public MyListItem(String title, String description, String note) {
        this.title = title;
        this.description = description;
        this.note = note;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getNote() {
        return note;
    }
}
